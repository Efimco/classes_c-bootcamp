﻿using System;
using UnityEngine;
using TMPro;

public class Logger : MonoBehaviour
{
    private string _message;
    public TextMeshProUGUI InputFieldText;
    public TextMeshProUGUI IlertText;

    public string Message
    {
        get
        {
            if (_message == "")
            {
                Debug.Log("The Field is empty");
                return null;
            }
            else
            {
                return _message;
            }
        }
        set
        {
            if (Message == "")
            {
                Debug.Log("Field is Empty");
            }
            else
            {
                _message = value;
            }
        }
    }

    private void Update()
    {
    }
}