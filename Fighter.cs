﻿[System.Serializable]
public class Fighter
{
    public int dmg;
    private int hp;

    public int Hp
    {
        get => hp;
    }

    public void ReciveDamage(int damage)
    {
        hp -= damage;
    }

    public int DealDamage()
    {
        return this.dmg;
    }

    public Fighter(int _dmg, int _hp)
    {
        this.hp = _hp;
        this.dmg = _dmg;
    }
}