﻿using UnityEngine;

public class NPC
{
    public int harism;
    public int intelegence;
    public int endurance;

    public NPC(int _harism)
    {
        this.harism = _harism;
    }

    public NPC(int _harism, int _intelegence, int _endurance)
    {
        this.harism = _harism;
        this.intelegence = _intelegence;
        this.endurance = _endurance;
    }

    public void Write()
    {
        Debug.Log(harism + " " + intelegence + " "  + endurance);
    }
}