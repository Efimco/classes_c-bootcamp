﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class OOP_Controller : MonoBehaviour
{
    private Robot _robot = new Robot();
    private NPC npc_1 = new NPC(1);
    private NPC npc_2 = new NPC(1, 2, 3);
    private Fighter[] _fighters = new Fighter[3];
    int counter = 0;

    void Start()
    {
        Task_1();
        Task_2();
        Task_3_start();
    }

    private void Update()
    {
        Task_3_update();
    }

    private void Task_1()
    {
        Debug.Log(Robot.strength);
        Debug.Log(Robot.weight);
        Debug.Log(Robot.velocity);
        Robot.strength = 3;
        Robot.weight = 7;
        Robot.strength = 123;
        Debug.Log(Robot.strength);
        Debug.Log(Robot.weight);
        Debug.Log(Robot.velocity);
    }

    private void Task_2()
    {
        npc_1.Write();
        npc_2.Write();
        npc_1.harism = 2;
        npc_2.intelegence = 7;
        npc_1.Write();
        npc_2.Write();
    }

    private void Task_3_start()
    {
        for (int i = 0; i < _fighters.Length; i++)
        {
            _fighters[i] = new Fighter(Random.Range(0, 11), 100);
        }
    }

    private void Task_3_update()
    {
        _fighters[Random.Range(0, 3)].ReciveDamage(_fighters[Random.Range(0, 3)].DealDamage());

        for (int i = 0; i < _fighters.Length; i++)
        {
            if (_fighters[i].Hp > 0) ;
            {
                counter++;
            }
            if (i == _fighters.Length - 1 && counter == 1)
            {
                for (int j = 0; j < _fighters.Length; j++)
                {
                    if (_fighters[j].Hp > 0)
                    {
                        Debug.Log("Fighter " + j + " Wins");
                    }
                }
            }
        }
    }
}